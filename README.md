# Boilerplate-Web

[![License][license-img]](https://github.com/astraloverflow/boilerplate-web/blob/master/LICENSE)
[![Version][version-img]](https://github.com/astraloverflow/boilerplate-web/releases)
[![Last Commit][last-commit-img]](https://github.com/astraloverflow/boilerplate-web/commits/master)
[![Open Issues][issues-img]](https://github.com/astraloverflow/boilerplate-web/issues)

> Boilerplate for creating non-SPA websites with modern web technologies

---

## Requirements
- Node.js (LTS or newer).
- Latest NPM (comes included with Node.js) or Yarn.

## Quick Start

```shell
$ cd ~/dev/
$ npx degit astraloverflow/boilerplate-web#1.0.0 my-new-website
$ cd my-new-website
$ npm install
$ npm run dev
```

---

## NPM Scripts

### `npm run test`
- Runs [stylelint](https://stylelint.io) (using [stylelint-config-astraloverflow](https://github.com/astraloverflow/stylelint-config-astraloverflow)) and [eslint](https://eslint.org) (using [standardjs](https://standardjs.com)) to check files in `src/` for syntax and coding style errors.

### `npm run build`
- Builds Sass/SCSS files using node-sass with production optimizations outputting in the `dist/_styles/` folder.
- Builds Javascript files using webpack with babel (using preset-env) with production optimizations outputting in the `dist/_scripts/` folder.
- Copies over all other files to the `dist/` folder.

### `npm run dev`
- Builds Sass/SCSS files using node-sass with sourcemaps outputting in the `.local/dev_server/_styles/` folder.
- Builds Javascript files using webpack with babel (using preset-env) with sourcemaps outputting in the `.local/dev_server/_scripts/` folder.
- Copies over all other files to the `.local/dev_server/` folder.
- Starts a local development server using [Browser-Sync](https://browsersync.io) with live reloading.

---

## Package.json

It is HIGHLY recommended that you customize `package.json` with the details of your project, mainly the following fields:

```json5
{
  // Prevents your project from unintentionally being published to NPM.
  "private": true,
  // URL to your project's code repository.
  // If hosted on GitHub you can use the shorthand "user/repo"
  "repository": "",
  // Your project's name, as seen by NPM. See NPM docs for allowed characters.
  "name": "boilerplate-web",
  // Your project's description, as seen by NPM. Can be left blank.
  "description": "",
  // SPDX license identifier for your project's license.
  // The list of SPDX license identifiers can be found here: https://spdx.org/licenses/
  "license": "",
  // Your project's version number, as seen by NPM.
  "version": "0.0.0"
}
```

[license-img]: https://img.shields.io/github/license/astraloverflow/boilerplate-web.svg
[version-img]: https://img.shields.io/github/release/astraloverflow/boilerplate-web.svg
[last-commit-img]: https://img.shields.io/github/last-commit/astraloverflow/boilerplate-web.svg
[issues-img]: https://img.shields.io/github/issues-raw/astraloverflow/boilerplate-web.svg


const $ = import('jquery')
import('./bootstrap')

console.log('Should be true if ' + '%cjQuery' + '%c (full or slim) is imported: ' + `%c${!!$}`, 'color: red;', 'color: black;', 'color: blue;')
console.log('Should be true if ' + '%cjQuery slim' + '%c (not full) is imported: ' + `%c${!$.ajax}`, 'color: red;', 'color: black;', 'color: blue;')

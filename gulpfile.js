const mode = {
  production: process.env.NODE_ENV === 'production',
  development: process.env.NODE_ENV === 'development' || process.env.NODE_ENV !== 'production'
}
const gulp = require('gulp')
const webpack = require('webpack')
const browserSync = require('browser-sync')
let plugin = {
  if: require('gulp-if'),
  stylelint: require('gulp-stylelint'),
  eslint: require('gulp-eslint'),
  changed: require('gulp-changed'),
  sourcemaps: require('gulp-sourcemaps'),
  sass: require('gulp-sass'),
  webpack: require('webpack-stream')
}
plugin.sass.compiler = require('node-sass')

gulp.task('default', [
  'etc',
  'styles',
  'scripts'
])

gulp.task('dev_server', ['default'], () => {
  gulp.watch(['src/**/*', '!src/{_styles,_scripts}/**/*'], ['etc'])
  gulp.watch(['src/_styles/**/*'], ['styles'])
  gulp.watch(['src/_scripts/**/*'], ['scripts'])
  return browserSync({
    // Options found here: https://browsersync.io/docs/options
    open: false,
    reloadDelay: 1000,
    logPrefix: 'BrowserSync',
    server: {
      baseDir: './.local/dev_server/',
      directory: false
    }
  })
})

gulp.task('etc', () => gulp.src(['src/**/*', '!src/{_styles,_scripts}/**/*'])
  .pipe(plugin.if(mode.development, plugin.changed('./.local/dev_server/')))
  .pipe(plugin.if(mode.development, gulp.dest('./.local/dev_server/')))
  .pipe(plugin.if(mode.production, gulp.dest('./dist/')))
  .pipe(plugin.if(mode.development, browserSync.stream()))
)

gulp.task('test:styles', () => gulp.src(['src/_styles/**/*.scss'])
  .pipe(plugin.stylelint({
    reporters: [{ formatter: 'string', console: true }]
  }))
)

gulp.task('styles', ['test:styles'], () => gulp.src(['src/_styles/**/*.scss'])
  .pipe(plugin.if(mode.development, plugin.sourcemaps.init()))
  .pipe(plugin.sass({ outputStyle: 'expanded' }).on('error', plugin.sass.logError))
  .pipe(plugin.if(mode.development, plugin.sourcemaps.write('../_sourcemaps/')))
  .pipe(plugin.if(mode.production, require('gulp-postcss')([
    require('autoprefixer'),
    require('postcss-clean'),
    require('postcss-reporter')
  ])))
  .pipe(plugin.if(mode.development, gulp.dest('./.local/dev_server/_styles/')))
  .pipe(plugin.if(mode.production, gulp.dest('dist/_styles/')))
  .pipe(plugin.if(mode.development, browserSync.stream()))
)

gulp.task('test:scripts', () => gulp.src(['src/_scripts/**/*.js'])
  .pipe(plugin.eslint())
  .pipe(plugin.eslint.format('stylish'))
)

gulp.task('scripts', ['test:scripts'], () => gulp.src(['src/_scripts/entry.js'])
  .pipe(plugin.webpack({
    mode: mode.production ? 'production' : 'development',
    devtool: mode.development ? 'inline-source-map' : false,
    output: {
      filename: 'entry.js',
      chunkFilename: '[name].[contenthash:6].js',
      publicPath: '/_scripts/'
    },
    module: {
      rules: [
        {
          test: /\.js$/,
          exclude: /node_modules/,
          use: {
            loader: 'babel-loader',
            options: {
              presets: ['@babel/preset-env'],
              plugins: ['@babel/plugin-syntax-dynamic-import']
            }
          }
        }
      ]
    },
    resolve: {
      alias: {
        jquery$: 'jquery/dist/jquery.slim.js'
      }
    }
  }, webpack))
  .pipe(plugin.if(mode.development, plugin.sourcemaps.init({ loadMaps: true })))
  .pipe(plugin.if(mode.development, plugin.sourcemaps.write('../_sourcemaps/')))
  .pipe(plugin.if(mode.development, gulp.dest('./.local/dev_server/_scripts/')))
  .pipe(plugin.if(mode.production, gulp.dest('dist/_scripts/')))
  .pipe(plugin.if(mode.development, browserSync.stream()))
)
